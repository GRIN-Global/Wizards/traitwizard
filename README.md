## How to install the Crop Trait Wizard for Curator Tool

1. Download and copy the file "CropTraitWizard.dll" to the folder "C:\Program Files\GRIN-Global\GRIN-Global Curator Tool\Wizards"
2. Unblock the DLL file: right click the DLL file, select properties and check "Unblock"
3. Run Curator Tool

## Training Videos on YouTube
Go to [https://www.youtube.com/watch?v=5gj9svpSWL8&list=PLrY0roojbpPUSUYqpk-JxKTeei9nScEC2](https://www.youtube.com/watch?v=5gj9svpSWL8&list=PLrY0roojbpPUSUYqpk-JxKTeei9nScEC2)

## Source code
Go to [https://gitlab.com/CIP-Development/grin-global_client_international](https://gitlab.com/CIP-Development/grin-global_client_international)